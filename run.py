from flask import Flask, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import TextField, IntegerField, SubmitField
from stki_scripts.main import findSim
from stki_scripts.main import bow
from stki_scripts.main import findWord
from kataHubung import mawur

app = Flask(__name__)
app.config.update(dict(SECRET_KEY='12345'))


class SearchTask(FlaskForm):
    keyword = TextField('Keyword')
    search = SubmitField('Search')

def searchTask(form):
    keyword = form.keyword.data
    path_corpus = "./text"
    res = findSim(keyword, path_corpus)
    #res = {"title 1":0.3, "title 2":0.5, "title 3":1.3} # change the value here
    return res




@app.route('/', methods=['GET','POST'])
def main():
    # create form
    sform = SearchTask(prefix='sform')

    # get response
    data = {}
    hubung = {} #bikin dictionary buat nampung hasil fungsi kata hubung
    #simpanBow = {} #bikin dictionary buat nampung hasil fungsi bow
    if sform.validate_on_submit() and sform.search.data:
        data = searchTask(sform)
        hubung = mawur("./text/" + sform.keyword.data)
        

        
    
    # render HTML
    return render_template('tes.html', sform = sform, data = data, hubung = hubung)

if __name__=='__main__':
    app.run(debug=True)