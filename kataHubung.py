# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 23:52:41 2017

@author: Feedlyy
"""

import re
"""
text = "My name is Abder"
match_pattern = re.search(r'Abder', text)
print "terdapat pada kalimat ke " + match_pattern.group(0)
"""

def mawur(doc):
    frequency = {}
    document = open(doc, 'r')
    print_document = document.read()
    text_lower = print_document.lower()
    #match_pattern = re.findall(r'\b[a-z]{3,15}\b', text_lower)
    match_pattern = re.findall(r'\bdan\b|\bserta\b|\batau\b|\btetapi\b|\bmelainkan\b|\bpadahal\b|\bsedangkan\b|\bdengan\b|\byang\b|\bjika\b|\bkalau\b|\bagar\b|\bsehingga\b|\bbahwa\b|\bsama\b', text_lower) #menghitung frekuensi kata
    #match_pattern = re.findall(r'.', text_lower) #menghitung semua elemen yg ada di dalem text
    #match_pattern = re.findall(r'[a-z]', text_lower) #hitung semua abjad yg muncul
    
    for word in match_pattern :
        count = frequency.get(word,0) #disini ngambil key sama value, key = word/kata value index/frekuensinya
        frequency[word] = count + 1
        
    frequency_list = frequency.keys() #ini untuk mendapatkan keysnya
    mawur = {}
    for words in frequency_list:
        mawur[words] = frequency[words]
        
    return mawur

